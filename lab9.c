#include<stdio.h>
int swap(int *n1,int *n2)
{
	int temp;
	temp=*n1;
	*n1=*n2;
	*n2=temp;
	return 0;
}

int main()
{ 
	int num1,num2;
	printf("ENTER THE NUMBERS:\n");
	scanf("%d%d",&num1,&num2);
	printf("NUMBERS BEFORE SWAPPING:\n num1=%d AND num2=%d",num1,num2);
	swap(&num1,&num2);
	printf("\nNUMBERS AFTER SWAPPING:\n num1=%d AND num2=%d",num1,num2);
	return 0;
}
