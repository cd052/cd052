#include <stdio.h>
#include <math.h> 
int main()
{
    float a, b, c;
    float r1,r2;
    float discriminant;
    printf("Enter values of a, b, c of quadratic equation (aX^2 + bX + c):\n ");
    scanf("%f%f%f", &a, &b, &c);
    discriminant = (b * b) - (4 * a * c);
    switch(discriminant > 0)
    {
        case 1:
            /* If discriminant is positive */
            r1 = (-b + sqrt(discriminant)) / (2 * a);
            r2 = (-b - sqrt(discriminant)) / (2 * a);
            printf("Two distinct and real roots exists: %.2f and %.2f", 
                    r1, r2);
            break;

        case 0:
            /* If discriminant is not positive */
            switch(discriminant < 0)
            {
                case 1:
                    /* If discriminant is negative */
                   printf("THE ROOTS ARE IMAGINARY");
                   break;

                case 0:
                    /* If discriminant is zero */
                    r1=r2 = -b / (2 * a);
                    printf("Two equal and real roots exists: %.2f and %.2f", r1, r2);
                    break;
            }
    }

    return 0;
}
