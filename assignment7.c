#include <stdio.h>
int main()
{
  int i,f,l,mid,n,ele,a[100];

  printf("Enter number of elements\n");
  scanf("%d", &n);

  printf("Enter %d integers\n", n);

  for (i=0;i<n;i++)
    scanf("%d",&a[i]);

  printf("Enter value to find\n");
  scanf("%d", &ele);

  f = 0;
  l = n - 1;
  mid=(f+l)/2;

  while (f<=l) 
  {
    if (a[mid]<ele)
      f=mid+1;
    else if (a[mid]==ele)
    {
      printf("%d found at location %d.\n",ele,mid+1);
      break;
    }
    else
      l=mid-1;

    mid=(f+l)/2;
  }
  if (f>l)
    printf("Not found! %d isn't present in the list.\n",ele);

  return 0;
}

